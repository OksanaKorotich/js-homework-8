`use strict`

// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let paragraphs = document.querySelectorAll('p');
paragraphs.forEach(item => item.style.backgroundColor="#ff0000");

// 2. Знайти елемент із id="optionsList". Вивести у консоль.

let targetElem = document.getElementById('optionsList');
console.log(targetElem);

    // Знайти батьківський елемент та вивести в консоль.
    console.log(targetElem.parentElement);

    // Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

    let children;

    if(targetElem.hasChildNodes){

        children = targetElem.childNodes;

    };

    for(let i = 0; i < children.length; i++){
        console.log(children[i].nodeName, children[i].nodeType);
    };

// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph

let testParagraph = document.getElementById('testParagraph');
testParagraph.innerHTML = '<p>This is a paragraph<p/>';
console.log(testParagraph);

// 4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль.

let mainHeader = document.querySelector('.main-header').children;

    //  Кожному з елементів присвоїти новий клас nav-item.

for(let i = 0; i < mainHeader.length; i++){
    mainHeader[i].classList.add('nav-item');
};

console.log(mainHeader);
    // Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

let sectionTitleElem = document.querySelectorAll('.section-title');

for(let i = 0; i < mainHeader.length; i++){
    sectionTitleElem[i].classList.remove('section-title');
};


console.log(sectionTitleElem);










